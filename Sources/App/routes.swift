import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    router.post("JWT_ConsultaEstructura") { req -> JWT_GenericResponse in
        return JWT_ConsultaDocumentoController().getTreeSocioPro()
        //hola
    }

    router.post("JWT_ConsultaDocumento") { req -> JWT_DocumentoResponse in
        return JWT_ConsultaDocumentoController().getDocument()

    }

    router.post("JWT_ConsultaListadoProceso") { req -> JWT_GenericResponse in
        return JWT_ConsultaDocumentoController().getListadoProceso()

    }

    router.post("JWT_ConsultaListadoBusqueda") { req -> JWT_GenericResponse in
        return JWT_ConsultaDocumentoController().getListadoBusqueda()

    }

    // Example of configuring a controller
    let todoController = TodoController()
    router.get("todos", use: todoController.index)
    router.post("todos", use: todoController.create)
    router.delete("todos", Todo.parameter, use: todoController.delete)
    
    router.post("JWT_registraAsistenciaManual") { req -> RecordVisitResponse in
        return VisitsController().recordVisitResponse()

    }

}
