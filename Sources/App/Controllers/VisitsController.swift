//
//  VisitsController.swift
//  App
//
//  Created by ARIEL DIAZ on 3/24/19.
//

import Vapor

final class VisitsController {

    func recordVisitResponse() -> RecordVisitResponse {
        return RecordVisitResponse(
            authentication: true,
            cookie: nil,
            activeDevice: 0,
            error: false,
            idSession: nil,
            serverMessage: nil,
            serverTime: nil,
            serverUTCTime: nil,
            jsonString: nil

        )
    }

}
