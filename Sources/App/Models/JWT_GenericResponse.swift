//
//  JWT_ConsultaDocumentoResponse.swift
//  App
//
//  Created by ARIEL DIAZ on 12/22/18.
//

import Foundation
import Vapor

final class JWT_GenericResponse: Content {
    var cabResponse: CabResponse!
    var responseSocio: String!
    
    init(responseSocio: String, cabResponse: CabResponse) {
        self.responseSocio = responseSocio
        self.cabResponse = cabResponse
        
    }
}

final class JWT_DocumentoResponse: Content {
    var cabResponse: CabResponse!
    var documento: String!
    
    init(documento: String, cabResponse: CabResponse) {
        self.documento = documento
        self.cabResponse = cabResponse
        
    }
}

final class CabResponse: Content {
    var codResponse: Int!
    var mensResponse: String!
    
    init(codResponse: Int, mensResponse: String) {
        self.codResponse = codResponse
        self.mensResponse = mensResponse
        
    }
}
