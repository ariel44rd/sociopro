//
//  RecordVisitResponse.swift
//  App
//
//  Created by ARIEL DIAZ on 3/24/19.
//

import Vapor

final class RecordVisitResponse: Content {

    var authentication: Bool! = false
    var cookie: String?
    var activeDevice: Int!
    var error: Bool?
    var idSession: String?
    var serverMessage: String?
    var serverTime: String?
    var serverUTCTime: String?
    var jsonString: String?

    init(authentication: Bool, cookie: String?, activeDevice: Int, error: Bool?, idSession: String?, serverMessage: String?, serverTime: String?, serverUTCTime: String?, jsonString: String?) {
        self.authentication = authentication
        self.cookie = cookie
        self.activeDevice = activeDevice
        self.error = error
        self.idSession = idSession
        self.serverMessage = serverMessage
        self.serverTime = serverTime
        self.serverUTCTime = serverUTCTime
        self.jsonString = jsonString

    }

}
